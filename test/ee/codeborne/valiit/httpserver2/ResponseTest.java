package ee.codeborne.valiit.httpserver2;

import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class ResponseTest {


    @Test
    public void getResponseInBytes() throws Exception {
        Response response = new Response() {

            @Override
            public Map<String, Object> getHeaderContent() {
                Map<String, Object> header = new HashMap<>();
                header.put("key", "value");
                return header;

            }

            @Override
            public byte[] getBody() throws IOException {
                return "body".getBytes();
            }

            @Override
            public StatusCode getStatusCode() {
                return StatusCode.OK;

            }

        };

        assertArrayEquals(("HTTP/1.1 200 OK\nkey: value\n\nbody").getBytes(), response.getResponseInBytes());
        //assertEquals(("HTTP/1.1 200 OK\nkey: value\n\nbody"), new String (response.getResponseInBytes()));


    }

    @Test
    public void getResponseInBytesWithSpy() throws Exception {
        Response response = spy(Response.class);
        doReturn(singletonMap("key", "value")).when(response).getHeaderContent();
        doReturn("body".getBytes()).when(response).getBody();
        doReturn(StatusCode.OK).when(response).getStatusCode();

        assertArrayEquals(("HTTP/1.1 200 OK\nkey: value\n\nbody").getBytes(), response.getResponseInBytes());


    }

}








