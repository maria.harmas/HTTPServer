package ee.codeborne.valiit.httpserver2;

import org.junit.Test;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class HeadResponseTest {
    @Test
    public void headerResponseToHEAD() throws Exception {
        Response response = new HeadResponse(new Request(Collections.singletonList("HEAD /test.txt")));
        String result = "";
        for (Map.Entry<String, Object> entry : response.getHeaderContent().entrySet()) {
            result += entry.getKey() + ": " + entry.getValue() + "\n";
            String statusCode = response.getStatusCode().toString();

            assertEquals("OK", statusCode);
            assertEquals("Content-Length: 16\n", result);
            assertArrayEquals("".getBytes(), response.getBody());

        }
    }



    @Test
    public void fileNotFoundResponseToHEAD() throws Exception {
        Response response = new HeadResponse(new Request(Collections.singletonList("HEAD /test.txx")));
        String statusCode = response.getStatusCode().toString();

        assertEquals("NOT_FOUND", statusCode);

    }


}


