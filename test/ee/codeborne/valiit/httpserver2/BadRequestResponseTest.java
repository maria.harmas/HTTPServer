package ee.codeborne.valiit.httpserver2;

import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class BadRequestResponseTest {
    @Test
    public void badRequest() throws IOException {
        Response response = new GetResponse(new Request(Collections.singletonList("GET /test.txx")));

        String result = "";
        for (Map.Entry<String, Object> entry : response.getHeaderContent().entrySet()) {
            result += entry.getKey() + ": " + entry.getValue() + "\n";
            String statusCode = response.getStatusCode().toString();


            assertEquals("", result);
            assertArrayEquals(("").getBytes(), response.getBody());
            assertEquals("Not found", statusCode);

        }
    }
}