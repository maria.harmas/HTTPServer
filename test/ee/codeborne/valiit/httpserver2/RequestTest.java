package ee.codeborne.valiit.httpserver2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class RequestTest {
    @Test
    public void getMethod() throws Exception {
        Request request = new Request(Collections.singletonList("GET /test.txt"));
        assertEquals("GET", request.getMethod());

    }

    @Test
    public void getFileName() throws Exception {
        Request request = new Request(Collections.singletonList("HEAD /test.txt"));
        assertEquals("test.txt", request.getFileName());
    }
    @Test
    public void getHeaderLines() {
        List<String> headerLines = new ArrayList<>();
        headerLines.add("GET...");
        headerLines.add("key1 : value1");
        headerLines.add("key2: value2.1: value2.2");

        Request request = new Request(headerLines);

        Map<String, String> headers = request.getHeaders();
        assertEquals(2, headers.size());
        assertEquals("value1", headers.get("key1"));
        assertEquals("value2.1: value2.2", headers.get("key2"));
    }


}