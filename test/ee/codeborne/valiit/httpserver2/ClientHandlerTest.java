package ee.codeborne.valiit.httpserver2;

import org.junit.Test;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class ClientHandlerTest {
    class TestSocket extends Socket {


        private ByteArrayOutputStream ops = new ByteArrayOutputStream();

        @Override
        public InputStream getInputStream() {
            return new ByteArrayInputStream("GET /test.txt HTTP/1.1".getBytes());
        }

        @Override
        public OutputStream getOutputStream() {
            return ops;
        }


        public byte[] getBytes() { //abimeetod
            return ops.toByteArray();
        }

    }

    TestSocket clientSocket = new TestSocket();


  /*  @Test
    public void clientHandler() throws IOException {


        ClientHandler clientHandler = new ClientHandler() {
            @Override
            Response getResponse(String line) throws IOException {
                return new Response() {
                    @Override
                    public Map<String, Object> getHeaderContent() {
                        return null;
                    }

                    @Override
                    public byte[] getBody() throws IOException {
                        return new byte[0];
                    }

                    @Override
                    public StatusCode getStatusCode() {
                        return null;
                    }

                    @Override
                    public byte[] getResponseInBytes() throws IOException {
                        return "MyResponse".getBytes();
                    }
                };
            }
        };

        //TestClientHandler clientHandler = new TestClientHandler();
        clientHandler.handle(clientSocket);
        assertArrayEquals("MyResponse".getBytes(), clientSocket.getBytes());
    }*/


    @Test
    public void MockedClientHandler() throws IOException {


        Response mockedResponse = mock(Response.class);
        doReturn("MyResponse".getBytes()).when(mockedResponse.getResponseInBytes());


        assertArrayEquals("MyResponse".getBytes(), clientSocket.getBytes());


    }

    @Test
    public void getResponse() throws IOException {




        Response response = new ClientHandler().getResponse(singletonList("GET /..."));
        assertTrue(response instanceof GetResponse);


    }

    @Test
    public void headResponse() throws IOException {


        Response response = new ClientHandler().getResponse(singletonList("HEAD /..."));
        assertTrue(response instanceof HeadResponse);


    }

    @Test
    public void badRequestResponse() throws IOException {

        Response response = new ClientHandler().getResponse(singletonList(""));
        assertTrue(response instanceof BadRequestResponse);


    }


}



