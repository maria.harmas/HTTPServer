package ee.codeborne.valiit.httpserver2;

import org.junit.Test;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class GetResponseTest {

    @Test
    public void headerResponseToGet() throws Exception {
        Response response = new GetResponse(new Request(Collections.singletonList("GET /test.txt")));

        String result = "";
        for (Map.Entry<String, Object> entry : response.getHeaderContent().entrySet()) {
            result += entry.getKey() + ": " + entry.getValue() + "\n";

            String statusCode = response.getStatusCode().toString();


            assertEquals("Content-Length: 16\n", result);
            assertArrayEquals("TEST\ntest2\ntest3".getBytes(), response.getBody());
            assertEquals("OK", statusCode);


        }

    }

    @Test
    public void FileNotFoundResponseToGet() throws Exception {
        Response response = new GetResponse(new Request(Collections.singletonList("GET /test.txx")));


        String statusCode = response.getStatusCode().toString();
        assertEquals("NOT_FOUND", statusCode);



    }
    }
