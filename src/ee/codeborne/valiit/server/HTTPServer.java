package ee.codeborne.valiit.server;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


import static java.nio.charset.StandardCharsets.UTF_8;


public class HTTPServer {
    public static void main(String[] args) throws Exception {


        ServerSocket serverSocket = new ServerSocket(8808);

        System.out.println("On hold");

        while (true) {
            try
                    (Socket clientSocket = serverSocket.accept();
                     BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                     DataOutputStream ops = new DataOutputStream(clientSocket.getOutputStream())) {

                System.out.println("Connection established");

                String line = in.readLine();
                System.out.println(line);

                if (line != null) {
                    String response = line.split(" ")[1];
                    String fileName = response.split("/")[1];
                    System.out.println(fileName);


                    Path path = Paths.get("src", fileName);
                    if (path.toFile().exists()) {
                        byte[] data = Files.readAllBytes(path);
                        String respond = "HTTP/1.1 200\ncontent-type: " + Files.probeContentType(path) +
                                "\ncontent-length: " +data.length + "\n\n";
                        clientSocket.getOutputStream().write(respond.getBytes(StandardCharsets.UTF_8));
                        if ("GET".equals(line.split(" ")[0]))ops.write(data);
                    } else {
                        String respond = "HTTP/1.1 404 Not Found \n\n file not found";
                        clientSocket.getOutputStream().write(respond.getBytes(UTF_8));

                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}




