package ee.codeborne.valiit.httpserver2;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Request extends ClientHandler {
    private String method;
    private String fileName;
    private Map<String, String> headers = new LinkedHashMap<>();


    public Request(List<String> requestLine) {
        String line = requestLine.get(0);
        headers = headersToMap(requestLine);



        if (line.contains(" "))

        {

            method = line.split(" ")[0];
            fileName = line.split(" ")[1].split("/")[1];

        } else

        {
            fileName = "not found";
        }
    }


    public Map<String, String> headersToMap(List<String> headerLines) {
        headerLines = headerLines.subList(1, headerLines.size());


        for (String headerLine : headerLines) {
            String[] headerSplits = headerLine.split(":", 2);
            headers.put(headerSplits[0].trim(), headerSplits[1].trim());



        }
        return headers;

    }


    public String getMethod() {
        return method;
    }

    public String getFileName() {
        return fileName;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }
}



