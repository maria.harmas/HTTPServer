package ee.codeborne.valiit.httpserver2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ClientHandler {
    public void handle(Socket clientSocket) throws IOException {

        try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
             OutputStream ops = clientSocket.getOutputStream()) {

            List<String> requestLines = readRequestLines(in);
            if (!requestLines.isEmpty()) {

                Response response = getResponse(requestLines);

                ops.write(response.getResponseInBytes());
            }

            ops.flush();


        }
    }

    public List<String> readRequestLines(BufferedReader in) throws IOException {
        List<String> headerLines = new ArrayList<>();

        String lines;
        while ((lines = in.readLine()) != null) {
            headerLines.add(lines);
            return headerLines;
        }
        return headerLines;

    }


    Response getResponse(List<String> requestLines) throws IOException {

        Request request = new Request(requestLines);
        Response response;



        if (("GET").equals(request.getMethod())) {
            response =  new GetResponse(request);
        } else if (("HEAD").equals(request.getMethod())) {
            response = new HeadResponse(request);
        } else {
            response = new BadRequestResponse();
        }
        return response;


    }
}
