package ee.codeborne.valiit.httpserver2;

import java.io.IOException;
import java.nio.file.Files;

public class GetResponse extends HeadResponse {
    public GetResponse(Request request) throws IOException {
        super(request);
    }

    @Override
    public byte[] getBody() throws IOException {
        if ("GET".equals(request.getMethod())) {
            return Files.readAllBytes(path);
        } else {
            return new byte[0];
        }

    }


}