package ee.codeborne.valiit.httpserver2;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;


public class HeadResponse extends Response {
    Path path;
    Request request;


    public HeadResponse(Request request) throws IOException {
        path = Paths.get("src", request.getFileName());
        this.request = request;

    }
    @Override
    public final Map<String, Object> getHeaderContent()  {
        Map<String, Object> headerContent = new HashMap<>();
        if (path.toFile().exists()) {
            headerContent.put("Content-Length", path.toFile().length());
            return headerContent;

        }
        return headerContent;
    }
    @Override
    public byte[] getBody() throws IOException {
        return new byte[0];

    }
    @Override
    public final StatusCode getStatusCode(){
        if (path.toFile().exists()) {
            return StatusCode.OK;
        }else{
            return StatusCode.NOT_FOUND;
        }
    }
}







