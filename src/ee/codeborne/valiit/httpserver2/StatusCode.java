package ee.codeborne.valiit.httpserver2;

public enum StatusCode {
    OK(200, "OK"), NOT_FOUND(404, "Not Found"), BAD_REQUEST(400, "Bad Request");

    private int code;
    private String message;


    public int getCode() {
    return code;

    }
    public String getMessage() {
        return message;
    }

    StatusCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}