package ee.codeborne.valiit.httpserver2;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

public class BadRequestResponse extends Response{
    @Override
    public Map<String, Object> getHeaderContent() {
        return Collections.emptyMap();
    }

    @Override
    public byte[] getBody() throws IOException {
        return new byte[0];
    }

    @Override
    public StatusCode getStatusCode() {
        return StatusCode.BAD_REQUEST;
    }
}
