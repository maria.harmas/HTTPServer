package ee.codeborne.valiit.httpserver2;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;


public abstract class Response {


    public abstract Map<String, Object> getHeaderContent();

    public abstract byte[] getBody() throws IOException;

    public abstract StatusCode getStatusCode();


    public byte[] getResponseInBytes() throws IOException {

        String result ="";
        for (Map.Entry<String, Object> entry : getHeaderContent().entrySet()) {
            result += entry.getKey() + ": " + entry.getValue() + "\n";
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        String statusLine = "HTTP/1.1 " + getStatusCode().getCode()+ " " + getStatusCode().getMessage();


        byte[] headerInBytes = result.getBytes();

        bos.write(statusLine.getBytes());
        bos.write("\n".getBytes());

        bos.write(headerInBytes);
        bos.write("\n".getBytes());
        bos.write(getBody());

        return bos.toByteArray();
    }
}


