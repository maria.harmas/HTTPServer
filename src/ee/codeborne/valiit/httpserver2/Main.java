package ee.codeborne.valiit.httpserver2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Main {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8880);


        while (true) {
            try (Socket clientSocket = serverSocket.accept()) {
                ClientHandler clientHandler = new ClientHandler();
                clientHandler.handle(clientSocket);
            }
        }
    }


}
